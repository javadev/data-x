package com.tpt.business.handler.sentinel;

import com.tpt.common.dto.Result;
import com.tpt.common.dto.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;



@Component
public class CustomerFallbackHandler {

    private static Logger logger = LoggerFactory.getLogger(CustomerFallbackHandler.class);
    


    public static Result fallBack(String name) {
        String str = "fallBack 我被限流了啊啊啊啊啊 ,参数=" + name + "," + LocalDateTime.now().toLocalTime().toString();

        logger.error(str);

        return ResultUtil.getFailureResult("500", str);
    }




}
