package com.tpt.business.handler.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.tpt.common.dto.Result;
import com.tpt.common.dto.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CustomerBlockHandler {

    private static Logger logger = LoggerFactory.getLogger(CustomerBlockHandler.class);



    public static Result limitBlockHandler(String  name,BlockException exception) {
        String str = "limitBlockHandler 我被限流了啊啊啊啊啊 ,参数=" +name +"," + LocalDateTime.now();

        logger.error(str);

        return ResultUtil.getFailureResult("500", str);
    }



}
