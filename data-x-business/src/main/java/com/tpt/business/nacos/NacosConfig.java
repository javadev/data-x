package com.tpt.business.nacos;

import com.alibaba.nacos.api.exception.NacosException;

public interface NacosConfig {

    //降级开关value
    public  static final   String  IS_OPEN_DEMOTION_CONFIG_Y = "Y";

    //降级开关key
    public  static final   String  IS_OPEN_DEMOTION_CONFIG = "isOpenDemotionConfig";

    public  void onReceiveNacosMessage() throws NacosException;

    public void receiveHandleInfo(String configInfo);
}
