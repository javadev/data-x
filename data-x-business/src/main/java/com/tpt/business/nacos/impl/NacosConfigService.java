package com.tpt.business.nacos.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.AbstractListener;
import com.alibaba.nacos.api.exception.NacosException;
import com.google.common.collect.Maps;
import com.tpt.business.nacos.NacosConfig;
import com.tpt.common.enums.NacosMetaData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class NacosConfigService implements NacosConfig {

    private Logger logger = LoggerFactory.getLogger(NacosConfigService.class);

    private Map<String, Object> tptMap ;

    @NacosInjected
    private ConfigService configService;

    @PostConstruct
    public void init() {
        try {
            String  config = configService.getConfig(NacosMetaData.DATAX_TPT_CONFIG.getDataId(),
                    NacosMetaData.DATAX_TPT_CONFIG.getGroup(), NacosMetaData.DATAX_TPT_CONFIG.getTimeout());
            receiveHandleInfo(config);
            onReceiveNacosMessage();
        } catch (NacosException e) {
            logger.error("ERROR IN   NacosConfigService  init  failed  ");
        }
    }


    @Override
    public  void onReceiveNacosMessage() throws NacosException {
        configService.addListener(NacosMetaData.DATAX_TPT_CONFIG.getDataId(), NacosMetaData.DATAX_TPT_CONFIG.getGroup(), new AbstractListener() {
            @Override
            public void receiveConfigInfo(String configInfo) {
                receiveHandleInfo(configInfo);
            }
        });
    }

    @Override
    public void receiveHandleInfo(String configInfo) {
        logger.error(String.format(" nacos NacosConfigService update info : %s   ", configInfo));
        if (StringUtils.isBlank(configInfo)) {
            logger.error(NacosMetaData.DATAX_TPT_CONFIG.getDataId() + " config is empty");
            return;
        }
        try {
            if (StringUtils.isNotBlank(configInfo)) {
                tptMap = JSONObject.parseObject(configInfo, Map.class);
            } else {
                tptMap = Maps.newHashMap();
            }
        } catch (Exception e) {
            logger.error("ERROR in receiving nacos config, dataId: " + NacosMetaData.DATAX_TPT_CONFIG.getDataId() + ", configInfo: " + configInfo, e);
        }
    }


    /**
     *  获取nacos 配置
     * @return
     */
    public Map<String, Object> getNacosMap() {
        return tptMap;
    }




}
