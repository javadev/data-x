package com.tpt.common.dto;

public enum ResultFlagEnum {

    SUCCESS("SUCCESS", "执行成功"),
    FAIL("FAIL", "执行失败"),
    ;

    public String execCode ;
    public String execMsg;


    ResultFlagEnum(String execCode , String execMsg) {
        this.execCode = execCode;
        this.execMsg = execMsg;
    }

    public String getExecCode() {
        return execCode;
    }

    public void setExecCode(String execCode) {
        this.execCode = execCode;
    }

    public String getExecMsg() {
        return execMsg;
    }

    public void setExecMsg(String execMsg) {
        this.execMsg = execMsg;
    }
}
