package com.tpt.common.dto;

public enum DataCenterErrorEnum {

    SYSTEM_ERROR("SYSTEM_ERROR", "系统错误"),
    PARAM_ERROR("PARAM_ERROR", "参数错误"),
    DB_PARAM_ERROR("DB_PARAM_ERROR", "DB插异常"),
    DATA_PROCESS("DATA_PROCESS", "数据处理中");

    public String errorCode ;
    public String errorMsg;


    DataCenterErrorEnum(String errorCode , String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


}
