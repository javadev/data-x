package com.tpt.common.dto;

public class ResultUtil {



    /**
     * 创建结果对象
     *
     * @author  tangpt
     * @param errorCode 错误码
     * @param errorMsg 错误描述
     * @param isSuccess 是否成功
     */
    public static Result getResult(String errorCode, String errorMsg, boolean isSuccess) {
        return new Result(isSuccess, errorCode, errorMsg);
    }

    /**
     * 创建失败结果集对象
     *
     * @author  tangpt
     * @param errorCode 错误码
     * @param errorMsg 错误描述
     */
    public static Result getFailureResult(String errorCode, String errorMsg) {
        return new Result(Boolean.FALSE, errorCode, errorMsg);
    }

    /**
     * 创建失败结果集对象
     *
     * @author  tangpt
     */
    public static Result getFailureResult(DataCenterErrorEnum dataCenterErrorEnum, String errorMsg) {
        return new Result(Boolean.FALSE, dataCenterErrorEnum.getErrorCode(), errorMsg);
    }

    /**
     * 创建失败结果集对象
     *
     */
    public static Result getFailureResult(DataCenterErrorEnum dataCenterErrorEnum, String errorMsg, String responseJson) {
        Result result = new Result(Boolean.FALSE, dataCenterErrorEnum.getErrorCode(), errorMsg);
        result.setData(responseJson);
        return result;
    }

    /**
     * 构造含对象的Result
     *
     * @author tangpt
     */
    public static Result getFailureResult(DataCenterErrorEnum dataCenterErrorEnum, String errorMsg, Object data) {
        Result result = new Result(Boolean.FALSE, dataCenterErrorEnum.getErrorCode(), errorMsg);
        result.setData(data);
        return result;
    }

    /**
     * 创建失败结果集对象
     *
     *  错误码枚举
     */
    public static Result getFailureResult(DataCenterErrorEnum dataCenterErrorEnum) {
        return new Result(Boolean.FALSE, dataCenterErrorEnum.getErrorCode(), dataCenterErrorEnum.getErrorMsg());
    }



    /**
     * 创建成功结果集对象
     *
     * @author tangpt
     */
    public static Result getSuccessResult() {
        return new Result(Boolean.TRUE, null, null);
    }

    /**
     * 创建成功结果集对象
     *
     * @param data
     *            结果集
     */
    public static Result getSuccessResult(Object data) {
        Result result = new Result(Boolean.TRUE, null, null);
        result.setData(data);
        return result;
    }
}
