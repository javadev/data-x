package com.tpt.common.enums;




/**
 * nacos 配置中心
 *  后缀是存储的格式，
 */
public enum NacosMetaData {

    DATAX_TPT_CONFIG("com.tpt.json","DEFAULT_GROUP",3000),


    ;


    private String dataId;
    private String group;
    private Integer  timeout;


    private NacosMetaData(String dataId, String group, Integer timeout) {
        this.dataId = dataId;
        this.group = group;
        this.timeout = timeout;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
