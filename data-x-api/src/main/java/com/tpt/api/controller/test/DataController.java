package com.tpt.api.controller.test;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.google.common.util.concurrent.RateLimiter;
import com.tpt.api.config.limitlua.Limit;
import com.tpt.api.config.limitlua.LimitType;
import com.tpt.api.controller.BaseController;
import com.tpt.business.handler.sentinel.CustomerBlockHandler;
import com.tpt.business.handler.sentinel.CustomerFallbackHandler;
import com.tpt.business.nacos.impl.NacosConfigService;
import com.tpt.common.dto.Result;
import com.tpt.common.dto.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;


@RestController
public class DataController  extends BaseController {


    private static Logger logger = LoggerFactory.getLogger(DataController.class);

    static RateLimiter rateLimiter = RateLimiter.create(10);

    @Resource
    private NacosConfigService nacosConfigService;


    @SentinelResource(value = "limiting", blockHandlerClass = CustomerBlockHandler.class, blockHandler = "limitBlockHandler", fallbackClass = CustomerFallbackHandler.class, fallback = "fallBack")
    @ResponseBody
    @GetMapping(value = "/limiting/{name}")
    public Result<Boolean> limiting(@PathVariable String name) {



        return ResultUtil.getSuccessResult(true);
    }


    @GetMapping(value = "/guavalimiting/{name}")
    public Result<Boolean> guavalimiting(@PathVariable String name) {
        if(!rateLimiter.tryAcquire()){
            String str = "guavalimiting 我被限流了啊啊啊啊啊 ,参数=" +name +"," + LocalDateTime.now();
            logger.error(str);
        }


        return ResultUtil.getSuccessResult(true);
    }


    @Limit(key = "customer_limit_test", period = 1, count = 10, limitType = LimitType.CUSTOMER)
    @GetMapping(value = "/redislimiting/{name}")
    public Result<Boolean> redislimiting(@PathVariable String name) {

        return ResultUtil.getSuccessResult(true);
    }

}
