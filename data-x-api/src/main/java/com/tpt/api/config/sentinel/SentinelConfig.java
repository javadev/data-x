package com.tpt.api.config.sentinel;

import com.alibaba.csp.sentinel.adapter.servlet.CommonFilter;
import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.ConfigurationPropertySources;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;


@Configuration
public class SentinelConfig implements BeanFactoryPostProcessor, EnvironmentAware {

    private ConfigurableEnvironment environment;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        Rules rules=resolverSetting();
        initRules(rules);
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = (ConfigurableEnvironment) environment;
    }

    private Rules resolverSetting() {

        Iterable<ConfigurationPropertySource> sources = ConfigurationPropertySources.get(environment);
        Binder binder = new Binder(sources);
        BindResult<Rules> bindResult = binder.bind("rules", Rules.class);
        Rules rules= bindResult.get();
        return  rules;
    }


    /**
     * 注解资源配置
     * @return
     */
    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }

    /**
     * web servlet适配
     * @return
     */
    @Bean
    public FilterRegistrationBean sentinelFilterRegistration() {
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new CommonFilter());
        registration.addUrlPatterns("/*");
        registration.setName("sentinelFilter");
        registration.setOrder(1);
        return registration;
    }


    public void initRules(Rules myRules){
        List<FlowRule> rules = new ArrayList<FlowRule>();
        for (Rule temp:myRules.getRuleList()) {
            FlowRule rule = new FlowRule();
            rule.setResource(temp.getResource());
            rule.setCount(temp.getCount());
            rule.setGrade(temp.getGrade());
            rule.setLimitApp(temp.getLimitApp());
            rules.add(rule);
        }
        FlowRuleManager.loadRules(rules);
    }

    /**
     * 降级规则
     * @param resource
     * RT_MAX_EXCEED_N：在降级策略为RT的情况下，如果连续RT_MAX_EXCEED_N个请求都大于配置的值，那么会在窗口时间内会进行降级状态，所有流量都会返回false(抛出 DegradeException)；在降级策略为异常比例的情况下，总qps且异常数大于该值才会进行异常比例的判断
     * count：降级策略为RT则表示响应时间；降级策略为异常比例则表示异常比例；降级策略为异常数则表示异常数量
     * timeWindow：降级的时间窗口，在该窗口时间内请求都不能通过
     * grade：降级熔断策略
     * cut：是否被降级熔断，如果true，则请求过来直接拒绝
     * passCount：降级策略为RT的时候用来统计超过配置值的数量
     */
    public static void initDegradeRule(String resource ,int ruleType ,int count ,int timeWindow ,int requestAmount) {
        List<DegradeRule> rules = new ArrayList<DegradeRule>();
        DegradeRule rule = new DegradeRule();
        rule.setResource(resource);
        // set limit exception count to 4
        rule.setCount(count);

        rule.setGrade(ruleType);
        rule.setTimeWindow(timeWindow);
        rule.setMinRequestAmount(requestAmount);
        rules.add(rule);
        DegradeRuleManager.loadRules(rules);
    }



}
