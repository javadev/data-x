package com.tpt.api.config.sentinel;

import lombok.Data;


@Data
public class Rule {
    private String resource;
    private Integer count;
    private Integer grade;
    private String limitApp;
}
