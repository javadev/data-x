package com.tpt.api.config.sentinel;

import lombok.Data;

import java.util.List;


@Data
public class Rules {

    private List<Rule> ruleList;

}


