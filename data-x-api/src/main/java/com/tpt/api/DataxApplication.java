package com.tpt.api;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySources;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.web.context.request.RequestContextListener;

import java.util.Date;
import java.util.TimeZone;


@SpringBootApplication
@ComponentScan(basePackages = {"com.tpt"})
@NacosPropertySources({
        @NacosPropertySource(dataId = "com.tpt.json", autoRefreshed = true)
})
public class DataxApplication {

    static {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
        SerializeConfig mapping = SerializeConfig.getGlobalInstance();
        mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setHashKeySerializer(RedisSerializer.string());
        redisTemplate.setHashValueSerializer(RedisSerializer.json());
        redisTemplate.setValueSerializer(RedisSerializer.json());
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        return redisTemplate;
    }



    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }


    public static void main(String[] args) {
        SpringApplication.run(DataxApplication.class,args);
    }

}
